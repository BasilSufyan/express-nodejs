const express = require('express');
const bodyParser = require('body-parser');

const leaderRouter = express.Router();
leaderRouter.use(bodyParser.json());

leaderRouter.route('/')
.all((req,res,next) => {
 	res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req,res,next) => {
	res.end('Will send all the leaders to you now!' );
})
.put((req,res,next) => {
	 res.statusCode = 403;
    res.end('PUT operation not supported on /leaders');
})
.post((req,res,next) => {
	res.end('Will add the promotion: ' + req.body.name + ' with new details: ' + req.body.description );
})
.delete((req,res,next) => {
	res.end('Deleting all leaders');
});

leaderRouter.route('/:leaderId')
.all((req,res,next) => {
 	res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req,res,next) => {
	res.end('Will send leader '+req.params.leaderId+' to you now!' );
})
.put((req,res,next) => {
	 res.statusCode = 403;
    res.end('PUT operation not supported on /leaders/'+req.params.leaderId);
})
.post((req,res,next) => {
	res.end('Will add the promotion: ' + req.body.name + ' with new details: ' + req.body.description );
})
.delete((req,res,next) => {
	res.end('Deleting the leader '+req.params.leaderId);
});


module.exports = leaderRouter;
const express = require('express');
const bodyParser = require('body-parser');

const promotionRouter = express.Router();
promotionRouter.use(bodyParser.json());

promotionRouter.route('/')
.all((req,res,next) => {
 	res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req,res,next) => {
	res.end('Will send all the promotions to you now!' );
})
.put((req,res,next) => {
	 res.statusCode = 403;
    res.end('PUT operation not supported on /promotions');
})
.post((req,res,next) => {
	res.end('Will add the promotion: ' + req.body.name + ' with new details: ' + req.body.description );
})
.delete((req,res,next) => {
	res.end('Deleting all promotions');
});


promotionRouter.route('/:promoId')
.all((req,res,next) => {
 	res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req,res,next) => {
	res.end('Will send all the promotion '+ req.params.promoId + 'to you now!' );
})
.put((req,res,next) => {
	 res.statusCode = 403;
    res.end('PUT operation not supported on /promotions/'+req.params.promoId);
})
.post((req,res,next) => {
	res.end('Will add the promotion ' +req.params.promoId + req.body.name + ' with new details: ' + req.body.description );
})
.delete((req,res,next) => {
	res.end('Deleting promotion '+req.params.promoId);
})

module.exports = promotionRouter;